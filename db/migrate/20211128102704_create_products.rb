class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :code, null: false
      t.string :name, null: false
      t.string :status, null: false, default: "draft"

      t.timestamps
    end
  end
end
