class UniqueIndexes < ActiveRecord::Migration[6.1]
  def change
    add_index :products, :code, unique: true
    add_index :products, :name, unique: true
    
    add_index :parts, :name, unique: true
    
    remove_index :part_products, :product_id
    add_index :part_products, [:product_id, :part_id], unique: true
    
    add_index :members, :code, unique: true
  end
end
