class ForeignKeyConstraints < ActiveRecord::Migration[6.1]
  def change
    add_foreign_key :part_products, :parts, on_delete: :restrict
    add_foreign_key :part_products, :products, on_delete: :cascade
  end
end
