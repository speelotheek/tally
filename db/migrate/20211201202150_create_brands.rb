class CreateBrands < ActiveRecord::Migration[6.1]
  def change
    create_table :brands, id: false do |t|
      t.string :name, primary_key: true

      t.timestamps
    end
    
    add_column :parts, :brand_name, :string
    add_column :products, :brand_name, :string
    
    add_foreign_key :parts, :brands, column: :brand_name, primary_key: :name, on_delete: :restrict
    add_foreign_key :products, :brands, column: :brand_name, primary_key: :name, on_delete: :restrict
  end
end
