class CreatePartProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :part_products do |t|
      t.references :part, null: false
      t.references :product, null: false
      t.integer :amount, null: false, default: 1

      t.timestamps
    end
  end
end
