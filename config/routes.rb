Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "products#index"
  
  resources :parts
  resources :products do
    member do
      get :pdf
    end
    resources :part_products
  end
end
