class PartsController < ApplicationController
  def index
    @parts = Part.order(:name)
  end

  def new
    @part = Part.new
  end

  def create
    part = Part.create!(part_params)

    if part.image.blank? && params[:part][:image_url].present?
      uri = URI(params[:part][:image_url])
      file = Net::HTTP.get(uri)

      part.image.attach(io: StringIO.new(file), filename: uri.path.slice(/[^\/]+\z/)) 
    end

    redirect_to parts_path
  end

  def edit
    part
  end

  def update
    part.update!(part_params)

    redirect_to parts_path
  end
  
  def destroy
    part.destroy!

    redirect_to parts_path
  end

  private

  def part
    @part ||= Part.find(params[:id])
  end

  def part_params
    params.require(:part).permit(:brand_name, :name, :image)
  end
end
