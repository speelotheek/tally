class PartProductsController < ApplicationController
  def new
    @part_product = PartProduct.new(product: product)
    @parts = Part.order(:name).pluck(:name, :id)
  end
  
  def create
    product.part_products.create!(part_product_params)

    redirect_to edit_product_path(product)
  end
  
  def edit
    product
    part_product
    @parts = Part.order(:name).pluck(:name, :id)
  end

  def update
    part_product.update!(part_product_params)

    redirect_to edit_product_path(product)
  end
  
  def destroy
    part_product.destroy!

    redirect_to edit_product_path(product)
  end

  private

  def part_product
    @part_product ||= PartProduct.find(params[:id])
  end

  def product
    @product ||= Product.find(params[:product_id])
  end

  def part_product_params
    params.require(:part_product).permit(:amount, :part_id)
  end
end
