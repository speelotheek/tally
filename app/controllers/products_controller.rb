# frozen_string_literal: true

class ProductsController < ApplicationController
  def index
    @products = Product.order(:name)
  end

  def show
    respond_to do |format|
      format.pdf do
        send_data(
          product.pdf,
          filename: "#{product.name}.pdf",
          disposition: 'inline',
          type: 'application/pdf'
        )
      end

      format.html do
        render :show, locals: { product: product }
      end
    end
  end

  def edit
    @part_products = product.part_products.includes(:part)
  end

  def update; end

  def destroy
    @product.destroy!

    redirect_to products_path
  end

  private

  def product
    @product ||= Product.find(params[:id])
  end
end
