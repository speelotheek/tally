# frozen_string_literal: true

class PartProduct < ApplicationRecord
  belongs_to :part
  belongs_to :product
end
