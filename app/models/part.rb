# frozen_string_literal: true

class Part < ApplicationRecord
  has_one_attached :image

  def image_path
    image.service.path_for(image.key)
  end
end
