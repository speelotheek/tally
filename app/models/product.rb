# frozen_string_literal: true

class Product < ApplicationRecord
  has_many :part_products
  has_many :parts, through: :part_products

  has_one_attached :pdf_cache

  def pdf
    (pdf_cache.presence || pdf_render).download
  end

  def pdf
    Pdf.render('app/views/products/show.pdf.erb', product: self)
  end

  private

  def pdf_render
    pdf_cache.attach(
      io: StringIO.new(Pdf.render('app/views/products/show.pdf.erb', product: self)),
      filename: "#{name}.pdf"
    )
  end
end
