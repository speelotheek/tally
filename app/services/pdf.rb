# frozen_string_literal: true

require 'pdfkit'
require 'weasyprint'

class Pdf
  OPTIONS = {
    page_size: 'A4',
    margin_top: '15mm',
    margin_left: '25mm',
    margin_right: '25mm',
    margin_bottom: '15mm',
    encoding: 'UTF-8'
  }.freeze

  class << self
    def render(template_path, variables = {}, _options = {})
      template = ERB.new(Rails.root.join(template_path).read, trim_mode: '%<>')
      html = template.result_with_hash(variables)
      WeasyPrint.new(html).to_pdf
    end
  end
end
